import { SearchService, NpmPackageInfo } from './search.service';
import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: ['input { margin-bottom: .5rem; }'],
  providers: [ SearchService ]
})
export class SearchComponent implements OnInit {
  withRefresh = false;
  packages$!: Observable<NpmPackageInfo[]>;
  private searchText$ = new Subject<string>();

  search(packageName: string) {
    this.searchText$.next(packageName);
  }

  ngOnInit() {
    this.packages$ = this.searchText$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(packageName =>
        this.searchService.search(packageName, this.withRefresh))
    );
  }

  constructor(private searchService: SearchService) { }


  toggleRefresh() { this.withRefresh = ! this.withRefresh; }

  getValue(event: Event): string {
    return (event.target as HTMLInputElement).value;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
