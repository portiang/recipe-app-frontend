import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { Recipes } from './recipes.model';
import { AuthService } from './../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RecipesService {
  private recipeUrl = 'https://api.staging.cont7.link/api/recipe/recipes/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    public authService: AuthService,
    private messageService: MessageService
  ) {}

  getRecipes(): Observable<Recipes[]> {
    return this.http.get<Recipes[]>(this.recipeUrl).pipe(
      tap((_) => this.log('fetched recipes')),
      catchError(this.handleError<Recipes[]>('getRecipes', []))
    );
  }

  addRecipe(recipe: Recipes): Observable<Recipes> {
    return this.http
      .post<Recipes>(this.recipeUrl, recipe, this.httpOptions)
      .pipe(
        tap((newRecipe: Recipes) =>
          this.log(`added recipe w/ id=${newRecipe.id}`)
        ),
        catchError(this.handleError<Recipes>('addRecipe'))
      );
  }

  deleteRecipe(id: number): Observable<Recipes> {
    const url = `${this.recipeUrl}/${id}`;

    return this.http.delete<Recipes>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted Recipe id=${id}`)),
      catchError(this.handleError<Recipes>('deleteRecipe'))
    );
  }

  updateRecipe(recipe: Recipes): Observable<any> {
    return this.http.put(this.recipeUrl, recipe, this.httpOptions).pipe(
      tap((_) => this.log(`updated hero id=${recipe.id}`)),
      catchError(this.handleError<any>('updateRecipe'))
    );
  }

  /*
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`RecipesService: ${message}`);
  }
}
