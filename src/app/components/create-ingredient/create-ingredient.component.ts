import { AuthService } from './../../services/auth.service';
import { Ingredients } from './../ingredients.model';
import { Component, OnInit } from '@angular/core';
import { IngredientsService } from '../ingredients.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-create-ingredient',
  templateUrl: './create-ingredient.component.html',
  styleUrls: ['./create-ingredient.component.css'],
})
export class CreateIngredientComponent implements OnInit {
  ingredients: Ingredients[] = [];

  constructor(private ingredientsService: IngredientsService, public authService: AuthService) {}

  ngOnInit(): void {
    this.getIngredients();
  }

  getIngredients(): void {
    this.ingredientsService
      .getIngredients()
      .subscribe((ingredients) => (this.ingredients = ingredients));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.ingredientsService
      .addIngredient({ name } as Ingredients)
      .subscribe((ingredients) => {
        this.ingredients.push(ingredients);
      });
  }

  delete(ingredient: Ingredients): void {
    this.ingredients = this.ingredients.filter((h) => h !== ingredient);
    this.ingredientsService.deleteIngredient(ingredient.id).subscribe();
  }
}
