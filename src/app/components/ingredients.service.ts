import { HandleError, HttpErrorHandler } from './../http-error-handler.service';
import { AuthService } from './../services/auth.service';
import { Ingredients } from './ingredients.model';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root',
})
export class IngredientsService {
  private ingredientUrl =
    'https://api.staging.cont7.link/api/recipe/ingredients/';
    private handleError: HandleError;

    constructor(
      private http: HttpClient,
      httpErrorHandler: HttpErrorHandler) {
      this.handleError = httpErrorHandler.createHandleError('HeroesService');
    }


  getIngredients(): Observable<Ingredients[]> {
    return this.http.get<Ingredients[]>(this.ingredientUrl)
      .pipe(
        catchError(this.handleError('getIngredients', []))
      );
  }

  searchIngredient(term: string): Observable<Ingredients[]> {
    term = term.trim();

    // Add safe, URL encoded search parameter if there is a search term
    const options = term ?
     { params: new HttpParams().set('name', term) } : {};

    return this.http.get<Ingredients[]>(this.ingredientUrl, options)
      .pipe(
        catchError(this.handleError<Ingredients[]>('searchIngredient', []))
      );
  }

  addIngredient(ingredient: Ingredients): Observable<Ingredients> {
    return this.http.post<Ingredients>(this.ingredientUrl, ingredient, httpOptions)
      .pipe(
        catchError(this.handleError('addIngredient', ingredient))
      );
  }


  deleteIngredient(id: number): Observable<unknown> {
    const url = `${this.ingredientUrl}${id}`; // DELETE api/heroes/42
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError('deleteIngredient'))
      );
  }


  updateIngredient(ingredient: Ingredients): Observable<Ingredients> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'my-new-auth-token');

    return this.http.put<Ingredients>(this.ingredientUrl, ingredient, httpOptions)
      .pipe(
        catchError(this.handleError('updateIngredient', ingredient))
      );
  }
}
