import { CreateIngredientComponent } from './components/create-ingredient/create-ingredient.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeComponent } from './components/recipe/recipe.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'recipe', component: RecipeComponent, canActivate: [AuthGuard]},
  {path: 'create/recipe', component: RecipeComponent, canActivate: [AuthGuard]},
  {path: 'create/ingredient', component: CreateIngredientComponent, canActivate: [AuthGuard]},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
